/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xsystems.servidorrest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
/**
 *
 * @author cgulden
 */
@SpringBootApplication
public class ServicioRest implements CommandLineRunner {

    private static final Logger LOGGER = LogManager.getLogger(ServicioRest.class);

    public static void main(String[] args) {
        SpringApplication.run(ServicioRest.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        LOGGER.info("Servidor Rest iniciado con exito ...!!!!");
    }
}
