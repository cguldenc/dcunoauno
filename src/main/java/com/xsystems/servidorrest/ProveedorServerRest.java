/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xsystems.servidorrest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author cgulden
 */
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE})
@Controller
public class ProveedorServerRest {

    private static final Logger LOG = LoggerFactory.getLogger(ProveedorServerRest.class);

    @RequestMapping("/")
    @ResponseBody
    String hello() {
        LOG.info("Hola Mundo!");
        return "Hola Mundo!";
    }

    @RequestMapping(value = "/enviarImgBase64", method = RequestMethod.GET)
    @ResponseBody
    String enviarImgBase64(String img) {
        LOG.info("ImgBase64String = " + img);
        LOG.info("Recepcion Ok!");
        return "Recepcion Ok!";
    }   

}
