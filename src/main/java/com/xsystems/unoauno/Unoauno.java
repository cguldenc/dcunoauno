/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xsystems.unoauno;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import java.io.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.commons.io.IOUtils;
import java.util.Date;
import javax.imageio.ImageIO;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author cgulden
 */
public class Unoauno {

    private static final Logger LOGGER = LogManager.getLogger(Unoauno.class);

    /**
     * @param args the command line arguments
     */
    String directorioDCam = "dcam/";
    String directorioUnoauno = "unoauno/";
    Integer pausaMls = 2000;
    boolean noExisteArchivo = true;
    String[] flist;
    File directory;
    Path pathOrigen;
    File destinoCopiado;
    File destinoImgPng;
    String secuencia;
    Integer color;
    String imgBase64Str;

    public void run() {
        iniciarTarea();
    }

    private void pausar() {
        try {
            Thread.sleep(pausaMls);
        } catch (InterruptedException e) {
            LOGGER.debug(e);
        }
    }

    private void iniciarTarea() {
        while (noExisteArchivo) {
            buscarArchivo();
        }
    }

    private void buscarArchivo() {
        pathOrigen = FileSystems.getDefault().getPath("").toAbsolutePath();
        directory = new File(pathOrigen.toString() + "\\src\\main\\resources\\dcam");
        MyFilenameFilter filter = new MyFilenameFilter(".tif");
        flist = directory.list(filter);
        if (flist.length == 0) {
            System.out.println("No hay archivo");
            pausar();
        } else {
            copiarArchivo();
            convertirImagenTiffaPng();
            convertirImagenPngAStringBase64();
            enviarImgAServidorRest();
            noExisteArchivo = false;
        }
    }

    private void copiarArchivo() {
        for (String flist1 : flist) {
            try {
                System.out.println("Archivo Encontrado : " + flist1);
                File origen = new File(directory + "/" + flist1);

                Date objDate = new Date();
                String strDateFormat = "yyyyMMddHHmmss";
                SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat);
                secuencia = objSDF.format(objDate);
                destinoCopiado = new File(pathOrigen.toString() + "\\src\\main\\resources\\copias" + "/" + "seq-" + secuencia + ".tif");

                FileUtils.copyFile(origen, destinoCopiado);
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(Unoauno.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    private void convertirImagenTiffaPng() {
        try {
            BufferedImage tiffImage = ImageIO.read(destinoCopiado);
            //escala grises
            //color = BufferedImage.TYPE_BYTE_GRAY;

            //escala blanco negro
            color = BufferedImage.TYPE_BYTE_BINARY;
            BufferedImage pngImage = new BufferedImage(tiffImage.getWidth(), tiffImage.getHeight(), color);
            // Draw image from original TIFF to the new JPEG image
            pngImage.createGraphics().drawImage(tiffImage, 0, 0, Color.WHITE, null);
            // Write the image as JPEG to disk
            destinoImgPng = new File(pathOrigen.toString() + "\\src\\main\\resources\\imgPng" + "/" + "seq-" + secuencia + ".png");
            ImageIO.write(pngImage, "png", destinoImgPng);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Unoauno.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void convertirImagenPngAStringBase64() {

        FileInputStream fileInputStreamReader;
        try {
            fileInputStreamReader = new FileInputStream(destinoImgPng);
            byte[] bytes = new byte[(int) destinoImgPng.length()];
            fileInputStreamReader.read(bytes);
            imgBase64Str = new String(Base64.encodeBase64(bytes), "UTF-8");
            System.out.println("Str=" + imgBase64Str);
        } catch (FileNotFoundException ex) {
            java.util.logging.Logger.getLogger(Unoauno.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(Unoauno.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void enviarImgAServidorRest() {
        Client client = Client.create();
        WebResource webResource1 = client.resource("http://localhost:8080/enviarImgBase64?img=" + imgBase64Str);
        ClientResponse response1 = webResource1.get(ClientResponse.class);
        System.out.println("Response = " + response1);
    }

    class MyFilenameFilter implements FilenameFilter {

        String end;

        public MyFilenameFilter(String end) {
            this.end = end;
        }

        public boolean accept(File dir, String name) {
            return name.endsWith(end);
        }
    }

}
