/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xsystems.main;

import com.xsystems.unoauno.Unoauno;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author cgulden
 */
public class Main {

    private static final Logger LOGGER = LogManager.getLogger(Main.class);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        LOGGER.debug("Iniciando servidor rest!");

        LOGGER.debug("Iniciando proceso uno a uno!");
        Unoauno unoauno = new Unoauno();
        unoauno.run();
       
    }

}
