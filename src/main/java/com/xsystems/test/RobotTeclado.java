/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.xsystems.test;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

/**
 *
 * @author cgulden
 */
public class RobotTeclado {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try {
            Robot robot = new Robot();

            /* Simulate a mouse click
            robot.mousePress(InputEvent.BUTTON1_MASK);
            robot.mouseRelease(InputEvent.BUTTON1_MASK);
             */
            // Simulate a key press
            robot.keyPress(KeyEvent.VK_D);
            //robot.keyRelease(KeyEvent.VK_A);           

        } catch (AWTException e) {
            e.printStackTrace();
        }

    }

}
